from random import randint

from AbstractStrategy import AbstractStrategy

# let's go left to right!
class BaseStrategy(AbstractStrategy):
    def __init__(self, game):
        AbstractStrategy.__init__(self, game)
        self._actions = ['left', 'right', 'turnleft', 'turnright', 'down', 'drop']
        self._step = 3
        self._rotation = -self._step #this will range from -5 to 5


    # needs work == goes too far. look at block width too
    def choose(self):
        if self._rotation < 0:
            moves = ['left' for _ in range(1,2*abs(self._rotation))]
        elif self._rotation == 0:
            moves = []
        else:
            moves = ['right' for _ in range(1,2*self._rotation)]

        if self._rotation >= self._step:
            self._rotation = -self._step
        else:
            self._rotation += 1

        moves.append('drop')

        return moves
